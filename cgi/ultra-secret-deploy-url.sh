#!/bin/bash
printf 'Content-type: text/html\n\n'

echo "<h1>Update and Deploy</h1>"
echo "<br>"
echo "<hr>"
echo "<samp>"
echo "" | /opt/website/update.sh 2>&1 | sed 's,$,<br>,g'
echo "</samp>"
echo "<br>"
echo "<hr>"

