#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#define DATADIR "../data"

#define BUFSIZE 1024
#define PATHBUFSIZE 128

#define UNUSED(x) (void)(x)

int main(int argc, char **argv, char **envp) {
	UNUSED(argc);
	UNUSED(argv);
	UNUSED(envp);
	unsigned char buf[BUFSIZE + 1];
	buf[0] = '\0';

	int c = 0;
	int count = 0;
	bool iserr = false;

	char * tmp = NULL;

	char pathbuf[PATHBUFSIZE +1];
	pathbuf[0] = '\0';

	FILE * f = NULL;

	// Test if POST Request
	if ((tmp = getenv("REQUEST_METHOD")) == NULL || (strcmp(tmp, "POST") != 0 && strcmp(tmp, "post") != 0)) {
		printf("Status: 405\r\n");
		printf("Content-type: text/html\r\n\r\n");
		printf("<strong>Error</strong>:  405 - Method Not Allowed\r\n");
		return 0;
	}

	// Read stdin to buf
	while (!iserr && (c = fgetc(stdin)) != EOF && c != '\0') {
		if (ferror(stdin)) {
			iserr = true;
			break;
		}
		if (!isdigit(c) && !isupper(c) && !islower(c) && c != '+' && c != '%' && c != '&' && c != '=' && c != '*' && c != '-' && c != '/' && c != '.' && c != '_') {
			iserr = true;
			break;
		}
		buf[count] = (unsigned char)c;
		count++;
		buf[count] = '\0';
		if (count >= BUFSIZE) {
			iserr = true;
			break;
		}
	}
	
	// Check if an error occurred during reading
	if (iserr) {
		printf("Status: 302\r\n");
		printf("Location: /fehler.html#error-bad-request\r\n");
		printf("Content-type: text/html\r\n\r\n");
		printf("<strong>Error</strong>:  400 - Bad Request\r\n");
		return 0;
	}

	// Build file path
	snprintf(pathbuf, PATHBUFSIZE, "%s/entry-%lu.txt", DATADIR, (unsigned long)time(NULL));
	// Write buf to file
	f = fopen(pathbuf, "w");
	if (f == NULL) {
		printf("Status: 302\r\n");
		printf("Location: /fehler.html#error-internal-server-error\r\n");
		printf("Content-type: text/html\r\n\r\n");
		printf("<strong>Error</strong>:  500 - Internal Server Error\r\n");
		return 0;
	}
	if (fputs((char*)buf, f) == EOF) {
		printf("Status: 302\r\n");
		printf("Location: /fehler.html#error-internal-server-error\r\n");
		printf("Content-type: text/html\r\n\r\n");
		printf("<strong>Error</strong>:  500 - Internal Server Error\r\n");
		fclose(f);
		return 0;
	}
	fclose(f);

	// Success!
	printf("Status: 302\r\n");
	printf("Location: /erfolg.html\r\n");
	printf("Content-type: text/html\r\n\r\n");
	printf("Hurra, alles hat funktioniert! Dein Antrag ist bei uns eingereicht.\r\n");

	return 0;
}
